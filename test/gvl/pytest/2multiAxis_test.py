import pytest

from motionTests import *

# def setup_module(Test_Motion):
#    print("Setup module")

# def setup_class(Test_Module):
#    print("Setup class")

# This module requires two axis that are setup and returned from a
# test fixture
# Test axis 1 has axis 2 as a master with ratio 2
class Test_multiMasterMotion:
    # Tests must start with the prefix: test
    def test_gearIn(self, testAxes, testPreMoveFixture, homeAndCenterAxes):
        assert gearIn(testAxes[1], master1=1, ratio1=-1.5) == True

    def test_checkGearInLatchedSettings(self, testAxes):
        assert checkGearInLatchedSettings(testAxes[1], 1, 1, -1.5) == True

    def test_checkGearInSlaveSettings(self, testAxes):
        assert checkGearInSlaveSettings(testAxes[0], testAxes[1], -1.5) == True

    def test_slaveAxisMove(self, testAxes):
        assert slaveAxisMove(testAxes[0], testAxes[1], -10) == True

    def test_slaveAxisLimitStopsMaster(self, testAxes):
        assert slaveAxisLimitStopsMaster(testAxes[0], testAxes[1]) == True

    def test_gearOut(self, testAxes):
        assert gearOut(testAxes[1]) == True

    # def test_errorPropegation

    # def test_masterAxisStop
