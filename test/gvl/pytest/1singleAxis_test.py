import pytest
from motionTests import *

"""
This file is runs tests to check the functionality of
tc_mca_std_lib on a 2 axis ESS test crate.
It uses the pytest framework to manage execute and manage
the tests. None of the tests are actuallt defined here,
this file is essentially a wrapper and the tests can be
found in motionTests.py. This is so that people can
customize the tests that they want to run by using the
tests from motionTests.
For example one person may want to test software on a
simulation plc where as another person might want to use
these tests to commission some hardware.
Useful options when invoking pytest are:
-s which prints text to consoles
-x stops program on first failed test
"""

# def setup_module(Test_Motion):
#    print("Setup module")

# def setup_class(Test_Module):
#    print("Setup class")


class Test_motion:
    # Tests must start with the prefix: test
    # def test_readNcParam()
    #    assert readNcParam()

    # def test_writeNcParam()
    #    assert writeNcParam()

    # def test_paraterizeAxis()
    #    assert parametizeAxis()

    def test_powerOn(self, testAxes, testPreMoveFixture, homeAndCenterAxes):
        assert powerOn(testAxes[0]) == True

    def test_powerOff(self, testAxes):
        assert powerOff(testAxes[0]) == True

    # def test_moveWhileDisabledResultsInError(self, testAxes):
    #    assert moveWhileDisabledResultsInError(testAxes[0]) == True

    def test_softLimitsOff(self, testAxes):
        assert softLimitsOff(testAxes[0]) == True

    def test_jogFwd(self, testAxes, testPreMoveFixture):
        assert jogFwd(testAxes[0]) == True

    def test_jogBwd(self, testAxes, testPreMoveFixture):
        assert jogBwd(testAxes[0]) == True

    # def test_moveWhileDisabled(self, testAxes)

    # Needs to have an error for fixutre
    # def test_resetAxis(self, testAxes, ):
    #    assert resetAxis(testAxes[0]) == True

    # def test_jogHalt

    # def test_jogStop

    def test_limitStopsJogFwd(self, testAxes, testPreMoveFixture):
        assert limitSwitchFwd(testAxes[0]) == True

    def test_limitStopsJogBwd(self, testAxes, testPreMoveFixture):
        assert limitSwitchBwd(testAxes[0]) == True

    def test_setSoftLimitFwdValue1(self, testAxes):
        assert setSoftLimitFwdValue(testAxes[0], 50.0) == True

    def test_setSoftLimitBwdValue1(self, testAxes):
        assert setSoftLimitBwdValue(testAxes[0], -50.0) == True

    def test_setSoftLimitFwdValue2(self, testAxes):
        assert setSoftLimitFwdValue(testAxes[0], 85.0) == True

    def test_setSoftLimitBwdValue2(self, testAxes):
        assert setSoftLimitBwdValue(testAxes[0], -85.0) == True

    def test_softLimitsOn(self, testAxes):
        assert softLimitsOn(testAxes[0]) == True

    def test_targetPositionMonitoringOn(self, testAxes):
        assert targetPositionWindowOn(testAxes[0]) == True

    # Reminder: Can't home to enc pulse on absolute encoder axis
    # def test_homing_HomeToEncPulse_Fwd(self, testAxes, testPreMoveFixture):
    #     assert home(testAxes[0], E_HomingRoutines.eHomeToEncPulse_Fwd, 70) == True

    def test_homing_HomeToRef_Fwd(self, testAxes, testPreMoveFixture):
        assert home(testAxes[0], E_HomingRoutines.eHomeToRef_Fwd, 70) == True

    def test_homing_HomeToLimit_Fwd(self, testAxes, testPreMoveFixture):
        assert home(testAxes[0], E_HomingRoutines.eHomeToLimit_Fwd, 70) == True

    def test_homing_HomeToRef_Bwd(self, testAxes, testPreMoveFixture):
        assert home(testAxes[0], E_HomingRoutines.eHomeToRef_Bwd, -70) == True

    # Reminder: Can't home to enc pulse on absolute encoder axis
    # def test_homing_HomeToEncPulse_Bwd(self, testAxes, testPreMoveFixture):
    #    assert home(testAxes[0], E_HomingRoutines.eHomeToEncPulse_Bwd, -70) == True

    def test_homing_HomeDirect(self, testAxes, testPreMoveFixture):
        assert homeDirect(testAxes[0], E_HomingRoutines.eHomeDirect, 10) == True

    def test_homing_HomeToLimit_Bwd(self, testAxes, testPreMoveFixture):
        assert home(testAxes[0], E_HomingRoutines.eHomeToLimit_Bwd, -70) == True

    def test_homingHalt(self, testAxes, testPreMoveFixture):
        assert homingHalt(testAxes[0], E_HomingRoutines.eHomeToRef_Fwd) == True

    def test_homingStop(self, testAxes, testPreMoveFixture):
        assert homingStop(testAxes[0], E_HomingRoutines.eHomeToRef_Fwd) == True

    # def test fHomeFinishPos

    # def test reset for real error

    # check that all motion function blocks make the bBusy go True

    # def test_homeStartingFromFwdLimitSwitch()

    # def test_homeStartingFromBwdLimitSwitch()

    def test_absolutePosition(self, testAxes, testPreMoveFixture, homeAndCenterAxes):
        assert absolutePosition(testAxes[0], -50) == True

    def test_absolutePositionHalt(self, testAxes, testPreMoveFixture):
        assert absolutePositionHalt(testAxes[0], 50) == True

    def test_absolutePositionStop(self, testAxes, testPreMoveFixture):
        assert absolutePositionStop(testAxes[0], 50) == True

    def test_absolutePosition_outOfRange(self, testAxes):
        assert absolutePosition_outOfRange(testAxes[0], 200) == True

    def test_relativePosition(self, testAxes, testPreMoveFixture):
        assert relativePosition(testAxes[0], 100) == True

    def test_relativePositionHalt(self, testAxes, testPreMoveFixture):
        assert relativePositionHalt(testAxes[0], -25) == True

    def test_relativePositionStop(self, testAxes, testPreMoveFixture):
        assert relativePositionStop(testAxes[0], -25) == True

    def test_relativePosition_outOfRange(self, testAxes, testPreMoveFixture):
        assert relativePosition_outOfRange(testAxes[0], 300) == True
