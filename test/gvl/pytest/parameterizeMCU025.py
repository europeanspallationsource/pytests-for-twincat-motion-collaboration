from motionFunctions import *
from eAxisParameters import E_AxisParameters

"""
These parameters are what I use for MCU025 which is a ESS test
crate model 1024.
CPU, 2 axis, 2 linear stages, 2 SSI encoders
"""


def parameterizeCrate(axis1, axis2):
    ### Parameterize Axis 1 for ESS test crate ###
    axis1.disableAxis()
    axis1.gearOut()
    axis1.resetAxis()
    # Speeds
    axis1.setNcAxisParam(E_AxisParameters.AxisMaxVelocity, 50)
    axis1.setNcAxisParam(E_AxisParameters.AxisRefVeloOnRefOutput, 600)
    # Encoder
    axis1.setNcAxisParam(E_AxisParameters.AxisEncoderScalingFactor, 60 / 4096)
#    axis1.setNcAxisParam(E_AxisParameters.AxisEncoderDirectionInverse, 0)
#    axis1.setNcAxisParam(E_AxisParameters.AxisEncoderOffset, -4405)
    # Motor
#    axis1.setNcAxisParam(E_AxisParameters.AxisMotorDirectionInverse, 1)
    # Soft limits
    axis1.setNcAxisParam(E_AxisParameters.AxisMaxSoftPosLimit, 80)
    axis1.setNcAxisParam(E_AxisParameters.AxisMinSoftPosLimit, -80)
    axis1.setNcAxisParam(E_AxisParameters.AxisEnMaxSoftPosLimit, 1)
    axis1.setNcAxisParam(E_AxisParameters.AxisEnMinSoftPosLimit, 1)
    # Position lag monitoring
    axis1.setNcAxisParam(E_AxisParameters.EnablePosLagMonitoring, 1)
    axis1.setNcAxisParam(E_AxisParameters.AxisMaxPosLagValue, 0.3)  # Error if using 0.1
    # Position range monitoring
    axis1.setNcAxisParam(E_AxisParameters.AxisEnPositionRangeMonitoring, 1)
    axis1.setNcAxisParam(E_AxisParameters.AxisPositionRangeWindow, 0.1)
    # Target position monitoring
    axis1.setNcAxisParam(E_AxisParameters.AxisEnTargetPositionMonitoring, 1)
    axis1.setNcAxisParam(E_AxisParameters.AxisTargetPositionWindow, 0.1)
    # Homing velocity
    axis1.setNcAxisParam(E_AxisParameters.AxisCalibrationVelocityForward, 15)
    axis1.setNcAxisParam(E_AxisParameters.AxisCalibrationVelocityBackward, 10)

    ### Parameterize Axis 2 for ESS test crate ###
    axis2.disableAxis()
    axis2.gearOut()
    axis2.resetAxis()
    # Speeds
    axis2.setNcAxisParam(E_AxisParameters.AxisMaxVelocity, 50)
    axis2.setNcAxisParam(E_AxisParameters.AxisRefVeloOnRefOutput, 600)
    # Encoder
    axis2.setNcAxisParam(E_AxisParameters.AxisEncoderScalingFactor, 60 / 4096)
#    axis2.setNcAxisParam(E_AxisParameters.AxisEncoderDirectionInverse, 1)
#    axis2.setNcAxisParam(E_AxisParameters.AxisEncoderOffset, 350)
    # Motor
#    axis2.setNcAxisParam(E_AxisParameters.AxisMotorDirectionInverse, 0)
    # Soft limits
    axis2.setNcAxisParam(E_AxisParameters.AxisMaxSoftPosLimit, 80)
    axis2.setNcAxisParam(E_AxisParameters.AxisMinSoftPosLimit, -80)
    axis2.setNcAxisParam(E_AxisParameters.AxisEnMaxSoftPosLimit, 1)
    axis2.setNcAxisParam(E_AxisParameters.AxisEnMinSoftPosLimit, 1)
    # Position lag monitoring
    axis2.setNcAxisParam(E_AxisParameters.EnablePosLagMonitoring, 1)
    axis2.setNcAxisParam(E_AxisParameters.AxisMaxPosLagValue, 0.2)  # Error if using 0.1
    # Position range monitoring
    axis2.setNcAxisParam(E_AxisParameters.AxisEnPositionRangeMonitoring, 1)
    axis2.setNcAxisParam(E_AxisParameters.AxisPositionRangeWindow, 0.1)
    # Target position monitoring
    axis2.setNcAxisParam(E_AxisParameters.AxisEnTargetPositionMonitoring, 1)
    axis2.setNcAxisParam(E_AxisParameters.AxisTargetPositionWindow, 0.1)
    # Homing velocity
    axis2.setNcAxisParam(E_AxisParameters.AxisCalibrationVelocityForward, 15)
    axis2.setNcAxisParam(E_AxisParameters.AxisCalibrationVelocityBackward, 10)

    return axis1, axis2
