from motionFunctions import *
from eAxisParameters import E_AxisParameters
import pyads

# A good way to think about testing is the three As
# Arrange, Act, Assert


def resetAxis(axis):
    # Need to write a real test here
    print(f"Test: Reset Axis")
    axis.resetAxis()
    return True


def powerOn(axis):
    print(f"Test: Power ON")
    axis.enableAxis()
    return axis.waitForStatusBit(axis.getEnabledStatus, True)


def powerOff(axis):
    print(f"Test Power OFF")
    axis.disableAxis()
    return axis.waitForStatusBit(axis.getEnabledStatus, False)


def softLimitsOff(axis):
    print(f"Test: Turn Soft Limits Off")
    axis.setSoftLimitsOff()
    softLimFwd = axis.getSoftLimitFwdEnableStatus()
    softLimBwd = axis.getSoftLimitBwdEnableStatus()
    return not softLimFwd and not softLimBwd


def softLimitsOn(axis):
    print(f"Test: Turn Soft Limits On")
    axis.setSoftLimitsOn()
    softLimFwd = axis.getSoftLimitFwdEnableStatus()
    softLimBwd = axis.getSoftLimitBwdEnableStatus()
    return softLimFwd and softLimBwd


"""
This test will calculate the time it should theoretically take for
the axis to get up to jog speed, it will then check that the velocity
is greater than zero. It then stops the jog and waits for the velocity
to be zero. The velocity zero check rounds the act vel to a number
of decimal places, by default 3.
"""


def jogFwd(axis):
    print(f"Test: Jog Forward")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    axis.jogFwd()
    # Wait for bBusy to be True
    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=2):
        return False

    if not axis.waitForStatusBit(axis.getMovingStatus, True, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bMoving failed to go TRUE upon jogging forward"
        )
        return False

    # Make sure jog doesn't result in an error
    if axis.getErrorStatus():
        return False
    # Check if positive velocity
    time.sleep(axis.calcTimeForAccel())
    if not axis.getActVel() > 0:
        return False
    axis.jogStop()
    axis.waitForStop(timeout=axis.calcTimeForDecel(marginOfSafety=2))
    return True


def jogBwd(axis):
    print(f"Test: Jog Backward")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    axis.jogBwd()
    # Wait for bBusy to be True
    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=2):
        return False

    if not axis.waitForStatusBit(axis.getMovingStatus, True, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bMoving failed to go TRUE upon jogging backward"
        )
        return False

    # Make sure jog doesn't result in an error
    if axis.getErrorStatus():
        return False
    # Check if negative velocity
    time.sleep(axis.calcTimeForAccel())
    if not axis.getActVel() < 0:
        return False
    axis.jogStop()
    axis.waitForStop(timeout=axis.calcTimeForDecel(marginOfSafety=2))
    return True


def setSoftLimitFwdValue(axis, softLimitFwdValue):
    print(f"Test: Set Soft Limit Forward Value")
    axis.setSoftLimitFwdValue(softLimitFwdValue)
    if axis.getSoftLimitFwdValue() == softLimitFwdValue:
        return True
    else:
        return False


def setSoftLimitBwdValue(axis, softLimitBwdValue):
    print(f"Test: Set Soft Limit Backward Value")
    axis.setSoftLimitBwdValue(softLimitBwdValue)
    if axis.getSoftLimitBwdValue() == softLimitBwdValue:
        return True
    else:
        return False


def targetPositionWindowOn(axis):
    print(f"Test: Set Target Position Window ON")
    axis.setAxisEnTargetPositionMonitoringON()
    return axis.getAxisEnTargetPositionMonitoring()


def limitSwitchFwd(axis):
    print(f"Test: Activate Limit Switch Forward")
    timeout = axis.calcTravelTimeForRange()
    axis.jogFwd()

    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=10):
        axis.jogStop()
        axis.waitForStop()
        print(f"bBusy didn't go high")
        return False

    returnValue = axis.waitForStatusBit(axis.getLimitFwd, False, timeout=timeout)
    axis.jogStop()
    axis.waitForStop()
    return returnValue


def limitSwitchBwd(axis):
    print(f"Test: Activate Limit Switch Backward")
    timeout = axis.calcTravelTimeForRange()
    axis.jogBwd()

    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=10):
        axis.jogStop()
        axis.waitForStop()
        print(f"bBusy didn't go high")
        return False

    returnValue = axis.waitForStatusBit(axis.getLimitBwd, False, timeout=timeout)
    axis.jogStop()
    axis.waitForStop()
    return returnValue


def homeDirect(axis, homeSequence, homePosition):
    print(f"Test: Homing Direct")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    axis.home(homeSequence, homePos=homePosition)

    if not axis.waitForStatusBit(axis.getHomedStatus, True, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bHomed failed to go TRUE after homing complete"
        )
        return False

    axis.waitForStatusBit(axis.getBusyStatus, True, timeout=1)
    axis.waitForStatusBit(axis.getBusyStatus, False, timeout=1)

    currentPos = axis.getActPos()

    if not currentPos == homePosition:
        print(
            f"Axis {axis.axisNum}: TEST FAIL - fPosition is {currentPos} was not set to fHomePos {homePosition}"
        )
        return False

    if not axis.waitForStatusBit(axis.getErrorStatus, False, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - Direct homing should not result in error"
        )
        return False

    return True


def home(axis, homeSequence, homePosition):
    print(f"Test: Homing with sequence {homeSequence.name}")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    axis.home(homeSequence, homePos=homePosition)
    timeout = axis.calcTravelTimeForRange()
    axis.waitForStatusBit(axis.getMovingStatus, True, timeout=2)
    if not axis.waitForStatusBit(axis.getHomedStatus, False, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bHomed failed to go FALSE upon starting home sequence"
        )
        return False
    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bBusy failed to go TRUE upon starting home sequence"
        )
        return False
    if not axis.waitForStatusBit(axis.getHomedStatus, True, timeout=timeout):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bHomed failed to go TRUE after homing complete"
        )
        return False
    return True


def homingHalt(axis, homeSequence):
    print(f"Test: Homing Abort")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    # Start homing
    axis.home(homeSequence)

    # Check test is executing
    if not axis.waitForStatusBit(axis.getHomedStatus, False, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bHomed failed to go FALSE upon starting home sequence"
        )
        return False
    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bBusy failed to go TRUE upon starting home sequence"
        )
        return False

    if not axis.waitForStatusBit(axis.getMovingStatus, True, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bMoving failed to go TRUE upon starting home sequence"
        )
        return False

    # Execute test
    axis.haltAxis()

    axis.waitForStatusBit(axis.getDeceleratingStatus, True, timeout=2)
    axis.waitForStatusBit(axis.getStandstillStatus, True, timeout=2)

    # Check post test conditions
    if not axis.waitForStop(timeout=axis.calcTimeForDecel(marginOfSafety=2)):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - Homing failed to reach 0 velocity after halt"
        )
        return False

    if not axis.waitForStatusBit(axis.getBusyStatus, False, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bBusy should go FALSE after homing abort"
        )
        return False

    if not axis.waitForStatusBit(axis.getHomedStatus, False, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bHomed should not be TRUE after homing abort"
        )
        return False
    if not axis.waitForStatusBit(axis.getCommandAbortedStatus, True, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bCommandAborted should be TRUE after homing abort"
        )
        return False
    if not axis.waitForStatusBit(axis.getErrorStatus, False, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - Homing abort using halt should not result in error"
        )
        return False
    return True


def homingStop(axis, homeSequence):
    print(f"Test: Homing Stop")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    # Start homing
    axis.home(homeSequence)

    # Check test is executing
    if not axis.waitForStatusBit(axis.getHomedStatus, False, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bHomed failed to go FALSE upon starting home sequence"
        )
        return False
    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bBusy failed to go TRUE upon starting home sequence"
        )
        return False

    if not axis.waitForStatusBit(axis.getMovingStatus, True, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bMoving failed to go TRUE upon starting home sequence"
        )
        return False

    # Execute test
    axis.stopAxis()

    axis.waitForStatusBit(axis.getDeceleratingStatus, True, timeout=2)
    axis.waitForStatusBit(axis.getStandstillStatus, True, timeout=2)

    # Check post test conditions
    if not axis.waitForStop(timeout=axis.calcTimeForDecel(marginOfSafety=2)):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - Homing failed to reach 0 velocity after halt"
        )
        return False

    if not axis.waitForStatusBit(axis.getBusyStatus, False, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bBusy should go FALSE after homing abort"
        )
        return False

    if not axis.waitForStatusBit(axis.getHomedStatus, False, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - bHomed should not be TRUE after homing abort"
        )
        return False
        # if not axis.waitForStatusBit(axis.getCommandAbortedStatus, True, timeout=5):
        #    print(
        #        f"Axis {axis.axisNum}: TEST FAIL - bCommandAborted should be TRUE after homing abort"
        #    )
        return False
    if not axis.waitForStatusBit(axis.getErrorStatus, True, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAIL - Homing abort using stop should result in error"
        )
        return False
    return True


def absolutePosition(axis, position, vel=None, acc=None, dec=None):
    print(f"Test: Absolute Position")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    if not vel == None:
        axis.setVelocity(vel)
    if not acc == None:
        axis.setAcceleration(acc)
    if not dec == None:
        axis.setDeceleration(dec)

    axis.moveAbsolute(position)
    timeout = axis.calcTravelTimeForMove()

    if not axis.waitForCommandDone(timeoutDoneTrue=30):
        print(f"Axis {axis.axisNum}: TEST FAILED - Command was not completed")
        return False

    if not axis.checkTargetPositionWindow():
        print(
            f"Axis {axis.axisNum}: TEST FAILED - Axis is not within target position window"
        )
        return False

    return True


def absolutePositionHalt(axis, position, vel=None, acc=None, dec=None):
    print(f"Test: Absolute Position Abort")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    if not vel == None:
        axis.setVelocity(vel)
    if not acc == None:
        axis.setAcceleration(acc)
    if not dec == None:
        axis.setDeceleration(dec)

    axis.moveAbsolute(position)
    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=2):
        return False

    time.sleep(axis.calcTimeForAccel())
    axis.haltAxis()
    axis.waitForStatusBit(axis.getDeceleratingStatus, True, timeout=2)
    axis.waitForStop(timeout=axis.calcTimeForDecel(marginOfSafety=2))

    if not axis.waitForStatusBit(axis.getBusyStatus, False, timeout=2):
        print(f"Axis {axis.axisNum}: bBusy should go False after halt")
        return False

    if not axis.waitForStatusBit(axis.getErrorStatus, False, timeout=2):
        print(f"Axis {axis.axisNum}: Should not set Error Status True after halt")
        return False

    if not axis.waitForStatusBit(axis.getCommandAbortedStatus, True, timeout=2):
        print(f"Axis {axis.axisNum}: Halt should set bCommandAborted to True")
        return False

    return True


def absolutePositionStop(axis, position, vel=None, acc=None, dec=None):
    print(f"Test: Absolute Position Stop ")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    # Start homing
    if not vel == None:
        axis.setVelocity(vel)
    if not acc == None:
        axis.setAcceleration(acc)
    if not dec == None:
        axis.setDeceleration(dec)

    axis.moveAbsolute(position)

    # Check test is executing
    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAILED - bBusy failed to go TRUE upon starting home sequence"
        )
        return False

    if not axis.waitForStatusBit(axis.getMovingStatus, True, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAILED - bMoving failed to go TRUE upon starting home sequence"
        )
        return False

    # Execute test
    axis.stopAxis()

    axis.waitForStatusBit(axis.getDeceleratingStatus, True, timeout=2)
    axis.waitForStatusBit(axis.getStandstillStatus, True, timeout=2)

    # Check post test conditions
    if not axis.waitForStop(timeout=axis.calcTimeForDecel(marginOfSafety=2)):
        print(
            f"Axis {axis.axisNum}: TEST FAILED - Homing failed to reach 0 velocity after halt"
        )
        return False

    if not axis.waitForStatusBit(axis.getBusyStatus, False, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAILED - bBusy should go FALSE after Stoping absolute position move"
        )
        return False

    if not axis.waitForStatusBit(axis.getCommandAbortedStatus, True, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAILED - bCommandAborted should be TRUE after Stoping absolute position move"
        )
        return False

    if not axis.waitForStatusBit(axis.getErrorStatus, False, timeout=5):
        print(
            f"Axis {axis.axisNum}: TEST FAILED - bError should be FALSE after Stoping absolute position move"
        )
        return False

    return True


def absolutePosition_outOfRange(axis, position, vel=None, acc=None, dec=None):
    print(f"Test: Absolute Position Out Of Range")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(
            f"Axis {axis.axisNum}: TEST FAILED - Error #{axis.getErrorID()} before test begins"
        )
        return False

    if not vel == None:
        axis.setVelocity(vel)
    if not acc == None:
        axis.setAcceleration(acc)
    if not dec == None:
        axis.setDeceleration(dec)

    axis.moveAbsolute(position)
    if not axis.waitForStatusBit(axis.getErrorStatus, True, timeout=2):
        print(
            f"Axis {axis.axisNum}: TEST FAILED - bError was not TRUE after trying to move to a position out of range"
        )
        return False

    return True


def relativePosition(axis, relativePos, vel=None, acc=None, dec=None):
    print(f"Test: Relative Position")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    if not vel == None:
        axis.setVelocity(vel)
    if not acc == None:
        axis.setAcceleration(acc)
    if not dec == None:
        axis.setDeceleration(dec)

    startPos = axis.getActPos()

    axis.moveRelative(relativePos)
    timeout = axis.calcTravelTimeForMove()

    if not axis.waitForCommandDone(timeoutDoneTrue=timeout):
        print(
            f"Axis {axis.axisNum}: TEST FAILED - Command not completed within timeout"
        )
        return False

    if not axis.checkTargetPositionWindow(targetPos=startPos + relativePos):
        print(
            f"Axis {axis.axisNum}: TEST FAILED - Did not reach the target position within target window"
        )
        return False

    return True


def relativePositionHalt(axis, position, vel=None, acc=None, dec=None):
    print(f"Test: Relative Position Abort\Halt")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    if not vel == None:
        axis.setVelocity(vel)
    if not acc == None:
        axis.setAcceleration(acc)
    if not dec == None:
        axis.setDeceleration(dec)

    axis.moveRelative(position)
    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=2):
        return False

    time.sleep(axis.calcTimeForAccel())
    axis.haltAxis()
    axis.waitForStatusBit(axis.getDeceleratingStatus, True, timeout=2)
    axis.waitForStop(timeout=axis.calcTimeForDecel(marginOfSafety=2))

    if not axis.waitForStatusBit(axis.getBusyStatus, False, timeout=2):
        print(f"Axis {axis.axisNum}: TEST FAILED - bBusy should go False after halt")
        return False

    if not axis.waitForStatusBit(axis.getErrorStatus, False, timeout=2):
        print(f"Axis {axis.axisNum}: TEST FAILED - Should not set Error Status True after halt")
        return False

    if not axis.waitForStatusBit(axis.getCommandAbortedStatus, True, timeout=2):
        print(f"Axis {axis.axisNum}: TEST FAILED - Halt should set bCommandAborted to True")
        return False

    return True


def relativePositionStop(axis, position, vel=None, acc=None, dec=None):
    print(f"Test: Relative Position Stop")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    if not vel == None:
        axis.setVelocity(vel)
    if not acc == None:
        axis.setAcceleration(acc)
    if not dec == None:
        axis.setDeceleration(dec)

    axis.moveRelative(position)
    if not axis.waitForStatusBit(axis.getBusyStatus, True, timeout=2):
        return False

    time.sleep(axis.calcTimeForAccel())
    axis.stopAxis()

    if not axis.waitForStatusBit(axis.getDeceleratingStatus, True, timeout=2):
        print(
            f"Axis {axis.axisNum}: Axis isn't decelerating after stopping relative move"
        )
        return False

    if not axis.waitForStop(timeout=axis.calcTimeForDecel()):
        print(f"Axis {axis.axisNum}: Axis isn't at rest after stopping relative move")
        return False

    if not axis.waitForStatusBit(axis.getBusyStatus, False, timeout=2):
        print(f"Axis {axis.axisNum}: bBusy should go False after halt")
        return False

    if not axis.waitForStatusBit(axis.getErrorStatus, False, timeout=2):
        print(f"Axis {axis.axisNum}: Stop command should set bError to True")
        return False

    return True


def relativePosition_outOfRange(axis, position, vel=None, acc=None, dec=None):
    print(f"Test: Relative Position Out Of Range")

    # Check precondition - should be no error
    if axis.getErrorStatus():
        print(f"Axis {axis.axisNum}: Has error #{axis.getErrorID()} before test begins")
        return False

    if not vel == None:
        axis.setVelocity(vel)
    if not acc == None:
        axis.setAcceleration(acc)
    if not dec == None:
        axis.setDeceleration(dec)

    axis.moveRelative(position)
    return axis.waitForStatusBit(axis.getErrorStatus, True, timeout=2)


def gearIn(
    axis,
    master1=None,
    ratio1=None,
    master2=None,
    ratio2=None,
    master3=None,
    ratio3=None,
    master4=None,
    ratio4=None,
):
    print(f"Test: Multi-master Gearing")
    axis.gearInMultiMaster(
        master1=master1,
        ratio1=ratio1,
        master2=master2,
        ratio2=ratio2,
        master3=master3,
        ratio3=ratio3,
        master4=master4,
        ratio4=ratio4,
    )

    return axis.waitForStatusBit(axis.getGearedStatus, True, timeout=2)


def gearOut(axis):
    print(f"Test: Gear Out")
    axis.gearOut()
    return axis.waitForStatusBit(axis.getGearedStatus, False, timeout=2)


def checkGearInLatchedSettings(axis, masterIndex, masterAxis, masterRatio):
    print(f"Test: Gear In Latched Settings")
    masterAxisLatched = axis.getMultiMasterAxisLatched(masterIndex)
    masterRatioLatched = axis.getMultiMasterRatioLatched(masterIndex)
    if masterAxisLatched == masterAxis and masterRatioLatched == masterRatio:
        return True
    else:
        return False


def checkGearInSlaveSettings(masterAxis, slaveAxis, gearRatio):
    print(f"Test: Gear In Slave Settings")
    slaveRatio = masterAxis.getMultiSlaveAxisRatio(slaveAxis.axisNum)
    if slaveRatio == gearRatio:
        return True
    else:
        return False


def slaveAxisMove(masterAxis, slaveAxis, relativePos):
    print(f"Test: Slave Axis Move")
    startPos = slaveAxis.getActPos()
    targetPos = (
        startPos + masterAxis.getMultiSlaveAxisRatio(slaveAxis.axisNum) * relativePos
    )
    masterAxis.moveRelativeAndWait(relativePos)
    return slaveAxis.checkTargetPositionWindow(targetPos=targetPos)


def slaveAxisLimitStopsMaster(masterAxis, slaveAxis):
    print(f"Test: Slave Axis Limit Stops Master")
    # Need to turn off soft limits here in order to test the switches
    slaveAxis.setSoftLimitsOff()
    masterAxis.jogBwd()

    if not masterAxis.waitForStatusBit(masterAxis.getBusyStatus, True, timeout=10):
        masterAxis.jogStop()
        masterAxis.waitForStop()

    timeout = masterAxis.calcTravelTimeForRange()
    slaveAxis.waitForStatusBit(slaveAxis.getLimitFwd, False, timeout=timeout)
    masterAxis.jogStop()
    return masterAxis.waitForStatusBit(masterAxis.getBwdEnabled, False, timeout=timeout)
