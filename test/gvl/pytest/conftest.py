import pyads
import pytest
import socket
from motionFunctions import *
from motionTests import *
from eAxisParameters import E_AxisParameters
from parameterizeMCU025 import parameterizeCrate

"""
conftest.py contains the fixtures for the pytest tests.
The testAxes fixture is intented to only be executed once
at the start of the code to set up the connection to the
plc and instantiate the axis. There is also code to close the
connection after the tests have completed. It returns the axes
objects as a tuple so that the tests can use them.
"""


@pytest.fixture(scope="session")
def testAxes(request):

    print("Test Fixture in conftest: Set up axis and PLC objects")

    # ***These should be modified for your application***
    PLC_AMS_NET_ID = "5.65.73.242.1.1"
    PLC_PORT = 852
    PLC_IP = "192.168.88.75"
    SENDER_AMS_NET_ID = "192.168.88.253.1.1"
    SENDER_IP = "192.168.88.253"
    # can probably be modified so the code finds the IP by itself

    plc1 = plc(
        PLC_AMS_NET_ID,
        PLC_PORT,
        PLC_IP,
        senderAmsNetId=SENDER_AMS_NET_ID,
        senderIp=SENDER_IP,
        hostname=socket.gethostname(),
    )
    plc1.connect()

    axis1 = axis(plc1, 1)
    axis2 = axis(plc1, 2)

    def fin():
        print("Teardown/finalizer: Disconnect from PLC")

        nonlocal plc1
        nonlocal axis1
        nonlocal axis2

        del plc1
        del axis1
        del axis2

    request.addfinalizer(fin)

    parameterizeCrate(axis1, axis2)

    return axis1, axis2


# Fixture for a pre move check
# Reset and attempt to enable axis
# Check axis is enabled otherwise return false
# Ensure there is no error otherwise return false
# Set vel, acc and dec for tests
@pytest.fixture(scope="function")
def testPreMoveFixture(testAxes):
    print("Test Fixture: Reset and enable axis")
    testAxes[0].resetAxis()
    if not (testAxes[0].waitForStatusBit(testAxes[0].getErrorStatus, False)):
        print("Fixture failed")
        return False
    testAxes[0].jogStop()
    testAxes[0].enableAxis()
    if not (testAxes[0].waitForStatusBit(testAxes[0].getEnabledStatus, True)):
        print("Fixture failed")
        return False
    if testAxes[0].getErrorStatus():
        print("Fixture failed")
        return False
    testAxes[0].setVelocity(15)
    testAxes[0].setAcceleration(100)
    testAxes[0].setDeceleration(100)

    testAxes[1].resetAxis()
    if not (testAxes[0].waitForStatusBit(testAxes[0].getErrorStatus, False)):
        print("Fixture failed")
        return False
    testAxes[1].jogStop()
    testAxes[1].enableAxis()
    if not (testAxes[1].waitForStatusBit(testAxes[1].getEnabledStatus, True)):
        print("Fixture failed")
        return False
    if testAxes[1].getErrorStatus():
        print("Fixture failed")
        return False
    testAxes[1].setVelocity(15)
    testAxes[1].setAcceleration(100)
    testAxes[1].setDeceleration(100)

    yield True

    print("Teardown for Fixture: Reset any errors")
    testAxes[0].resetAxis()
    testAxes[0].jogStop()
    testAxes[0].waitForStatusBit(testAxes[0].getErrorStatus, False)

    testAxes[1].resetAxis()
    testAxes[1].jogStop()
    testAxes[1].waitForStatusBit(testAxes[1].getErrorStatus, False)


@pytest.fixture(scope="function")
def homeAndCenterAxes(testAxes):
    print("Test Fixture: Home and centre")

    # Enable and check
    testAxes[0].enableAxis()
    testAxes[1].enableAxis()

    if not testAxes[0].waitForStatusBit(testAxes[0].getEnabledStatus, True):
        print("Fixture failed: Axis 1 did not enable")
        return False

    if not testAxes[1].waitForStatusBit(testAxes[1].getEnabledStatus, True):
        print("Fixture failed: Axis 2 did not enable")
        return False

    # Home and check
    testAxes[0].home(E_HomingRoutines.eHomeToRef_Fwd, homePos=-70)
    timeout0 = testAxes[0].calcTravelTimeForRange()
    testAxes[1].home(E_HomingRoutines.eHomeToRef_Bwd, homePos=-55)
    timeout1 = testAxes[1].calcTravelTimeForRange()

    if not testAxes[0].waitForStatusBit(
        testAxes[0].getDoneStatus, True, timeout=timeout0
    ):
        print("Fixture failed: Axis 1 did not home")
        return False
    if not testAxes[1].waitForStatusBit(
        testAxes[1].getDoneStatus, True, timeout=timeout0
    ):
        print("Fixture failed: Axis 2 did not home")
        return False

    # Center both axes and check
    testAxes[0].moveAbsolute(0)
    timeout0 = testAxes[0].calcTravelTimeForMove()
    testAxes[1].moveAbsolute(0)
    timeout1 = testAxes[1].calcTravelTimeForMove()

    if not testAxes[0].waitForStatusBit(
        testAxes[0].getDoneStatus, True, timeout=timeout0
    ):
        print("Fixture failed: Axis 1 did not move to 0")
        return False

    if not testAxes[1].waitForStatusBit(
        testAxes[1].getDoneStatus, True, timeout=timeout0
    ):
        print("Fixture failed: Axis 2 did not move to 0")
        return False

    yield True

    print("Teardown for Fixture: Reset any errors")
    testAxes[0].resetAxis()
    testAxes[0].jogStop()
    testAxes[0].waitForStatusBit(testAxes[0].getErrorStatus, False)

    testAxes[1].resetAxis()
    testAxes[1].jogStop()
    testAxes[1].waitForStatusBit(testAxes[1].getErrorStatus, False)
