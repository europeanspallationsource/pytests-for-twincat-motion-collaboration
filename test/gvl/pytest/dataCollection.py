from motionFunctions import *
import socket

import numpy as np
import matplotlib.pyplot as plt

PLC_AMS_NET_ID = "5.65.73.242.1.1"
PLC_PORT = 852
PLC_IP = "192.168.88.254"
SENDER_AMS_NET_ID = "192.168.88.253.1.1"
SENDER_IP = "192.168.88.253"

plc1 = plc(
    PLC_AMS_NET_ID,
    PLC_PORT,
    PLC_IP,
    senderAmsNetId=SENDER_AMS_NET_ID,
    senderIp=SENDER_IP,
    hostname=socket.gethostname(),
)
plc1.connect()

axis1 = axis(plc1, 1)
axis2 = axis(plc1, 2)


def main():
    axis1.resetAxis()
    axis1.waitForStatusBit(axis1.getErrorStatus, False)
    axis1.enableAxis()
    axis1.waitForStatusBit(axis1.getEnabledStatus, True)

    max = 70
    interval = 10
    sleepInt = 0.01
    setPos = np.arange(-max, max + interval, interval)
    actPos = np.zeros(len(setPos))

    for i in range(0, len(setPos)):
        axis1.setPosition(setPos[i])
        axis1.setMotionCommand(E_MotionFunctions.eMoveAbsolute)
        if setPos[i] != axis1.getPosition():
            return False
        axis1.executeAxis()
        # axis1.waitForCommandDone(timeoutDoneTrue=axis1.calcTravelTimeForMove())
        axis1.waitForCommandDone()
        actPos[i] = axis1.getActPos()

    plt.scatter(setPos, actPos)
    plt.title("XXX Title XXX")
    plt.xlabel("Set Pos")
    plt.ylabel("Act Pos")
    plt.show()

    axis1.disableAxis()


if __name__ == "__main__":
    main()
