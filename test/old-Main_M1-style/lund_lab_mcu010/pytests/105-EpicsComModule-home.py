#!/usr/bin/env python

#
# Test script to test the homing of a TwinCAT axis
#
import re
import os

try:
    import pyads as pyads

    use_minipyads = False
except ImportError:
    import minipyads.minipyads as pyads

    use_minipyads = True

import sys
import unittest

# from motor_lib import motor_lib
# lib = motor_lib()
# import capv_lib
###


class Test(unittest.TestCase):
    valid_url_short = "adsmain://hostip/amsnetid:amsport"
    valid_url_long = "adsmain://amsnetid:amsport"
    RE_MATCH_IP_AND_AMS_NET_ID = re.compile(
        r"adsmain://" "(\d+.\d+.\d+.\d+)/" "(\d+.\d+.\d+.\d+.\d+.\d+):(\d+)"
    )
    RE_MATCH_AMS_NET_ID_PORT = re.compile(
        r"adsmain://" "(\d+.\d+.\d+.\d+.\d+.\d+.\d+):(\d+)"
    )

    motor = os.getenv("TESTEDMOTORAXIS")
    print("motor=%s" % (motor))
    re_match_ip_and_ams_net_id = RE_MATCH_IP_AND_AMS_NET_ID.match(motor)
    re_match_ams_net_id_port = RE_MATCH_AMS_NET_ID_PORT.match(motor)
    ams_net_id = None
    if re_match_ip_and_ams_net_id != None:
        host_ip = re_match_ip_and_ams_net_id.group(1)
        ams_net_id = re_match_ip_and_ams_net_id.group(2)
        ams_port = int(re_match_ip_and_ams_net_id.group(3))
        print("host_ip=%s ams_net_id=%s ams_port=%d" % (host_ip, ams_net_id, ams_port))

    elif re_match_ams_net_id_port != None:
        host_ip = re_match_ams_net_id_port.group(1)
        ams_net_id = re_match_ams_net_id_port.group(2)
        ams_port = int(re_match_ams_net_id_port.group(3))
        print(
            "URL (%s). host_ip=%s ams_net_id=%s ams_port=%u"
            % (motor, host_ip, ams_net_id, ams_port)
        )
        if host_ip != "":
            print("Invalid URL (%s). Use e.g. %s" % (motor, valid_url_long))
            print("Invalid URL (%s). Use e.g. %s" % (motor, valid_url_short))
            sys.exit(1)

    if ams_net_id == None:
        print("Invalid URL (%s). Use e.g. %s" % (motor, valid_url_short))
        sys.exit(1)

    print(
        "Valid URL (%s). host_ip=%s ams_net_id=%s ams_port=%u"
        % (motor, host_ip, ams_net_id, ams_port)
    )

    if use_minipyads:
        plc = pyads.Connection(
            ams_net_id,
            ams_port,
            ip_address=host_ip,
            useStdOutForLogging=True,
            logDebug=True,
        )
    else:
        plc = pyads.Connection(ams_net_id, ams_port, ip_address=host_ip)

    plc.open()
    bEnabled = plc.read_by_name("Main.M1.bEnabled", pyads.PLCTYPE_BOOL)
    print("motor=%s bEnabled=%s type(bEnabled)=%s" % (motor, bEnabled, type(bEnabled)))

    bActPositon = plc.read_by_name("Main.M1.fActPosition", pyads.PLCTYPE_LREAL)
    print(
        "motor=%s bActPositon=%s type(bActPositon)=%s"
        % (motor, bActPositon, type(bActPositon))
    )

    # hlm = float(capv_lib.capvget(motor + '.HLM'))
    # llm = float(capv_lib.capvget(motor + '.LLM'))

    # range_postion    = hlm - llm
    # homing_velocity  = capv_lib.capvget(motor + '.HVEL')
    # acceleration     = capv_lib.capvget(motor + '.ACCL')

    # Home the motor
    def test_TC_105(self):
        motor = self.motor
        tc_no = "TC-105"
        print("%s Home the motor" % tc_no)
        msta = int(capv_lib.capvget(motor + ".MSTA"))
        if msta & lib.MSTA_BIT_PLUS_LS:
            capv_lib.capvput(motor + ".HOMR", 1)
        else:
            capv_lib.capvput(motor + ".HOMF", 1)
        time_to_wait = 30
        if self.range_postion > 0 and self.homing_velocity > 0:
            time_to_wait = (
                1 + self.range_postion / self.homing_velocity + 2 * self.acceleration
            )

        # Homing velocity not implemented, wait longer
        time_to_wait = 180
        done = lib.waitForStartAndDone(motor, tc_no, time_to_wait)

        msta = int(capv_lib.capvget(motor + ".MSTA"))
        self.assertEqual(True, done, "done = True")
        self.assertNotEqual(
            0, msta & lib.MSTA_BIT_HOMED, "MSTA.homed (Axis has been homed)"
        )
